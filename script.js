// Прототипное наследование позволяет нам создавать новые объекты на основе уже ранее созданого обекта. Также можно добавлять новые методы.



class Employee {
  constructor(options) {
    this.name = options.name;
    this.age = options.age;
    this.salary = options.salary;
  }

}

class Programmer extends Employee {
  constructor(options) {
    super(options)
    this.name = options.name;
    this.age = options.age;
    this.salary = options.salary;
    this.lang = options.lang;
  }

  get salaryInfo() {
    return this.salary * 3
  }
}

const programmer1 = new Programmer({
  name: 'Artem',
  age: 42,
  salary: 3000,
  lang: ['Python', 'JavaScript']
});

const programmer2 = new Programmer({
  name: 'Ivan',
  age: 41,
  salary: 1500,
  lang: ['Java', 'Angular']
});

const programmer3 = new Programmer({
  name: 'Gogi',
  age: 19,
  salary: 2000,
  lang: ['C#', 'Java']
});

console.log(programmer1, programmer1.salaryInfo);
console.log(programmer2, programmer2.salaryInfo);
console.log(programmer3, programmer3.salaryInfo);

